import mongoose from 'mongoose';

const UserTokenResetSchema = mongoose.Schema(
  {
    tokenPassword: {
      type: String
    },
    resetEmail: {
      tokenEmail: {
        type: String
      },
      updateEmail: {
        type: String,
        match: /@/i
      }
    }
  },
  {
    strict: true,
    _id: false
  }
);

export default UserTokenResetSchema;

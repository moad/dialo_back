import mongoose from 'mongoose';

const UserSessionSchema = mongoose.Schema({
  providerId: {
    type: String
  },
  provider: {
    type: String,
    enum: ['Dialogram', 'Facebook', 'Twitter', 'Google'],
    default: 'Dialogram'
  },
  token: {
    type: String,
    required: true
  },
  device: {
    userAgent: {
      type: String,
      default: null
    },
    OS: {
      type: String,
      default: null
    }
  }
});

export default UserSessionSchema;

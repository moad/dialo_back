import mongoose, { Schema } from 'mongoose';
import moment from 'moment';
import mongoosePaginate from 'mongoose-paginate';

import UserSessionSchema from 'models/user/userSessionSchema';
import UserProfileSchema from 'models/user/userProfileSchema';
import UserTokenResetSchema from 'models/user/userTokenResetSchema';
import UserSettingSchema from 'models/user/userSettingSchema';
import UserFeaturesSchema from 'models/user/userFeaturesSchema';
import bcrypt from 'bcrypt-nodejs';

const UserSchema = mongoose.Schema(
  {
    nickName: {
      type: String,
      unique: true,
      required: true,
      minlength: 5,
      maxlength: 15,
      trim: true,
      searchable: true
    },
    profile: {
      type: UserProfileSchema,
      required: true
    },
    email: {
      type: String,
      unique: true,
      required: true,
      match: /@/i
    },
    password: {
      type: String
    },
    timestamp: {
      type: Number,
      default: moment().unix(1318874398806)
    },
    features: {
      type: UserFeaturesSchema,
      default: {}
    },
    settings: {
      type: UserSettingSchema,
      default: {}
    },
    sessions: {
      type: [UserSessionSchema],
      default: []
    },
    tokenReset: {
      type: UserTokenResetSchema,
      default: {}
    }
  },
  {
    strict: true
  }
);

/*  Hash Password before save  */
UserSchema.pre('save', async function(next) {
  var user = this;

  if (!user.isModified('password')) return next();
  try {
    user.password = await bcrypt.hashSync(user.password);
    next();
  } catch (err) {
    return next(err);
  }
});

UserSchema.plugin(mongoosePaginate);
const User = mongoose.model('User', UserSchema);

export default User;

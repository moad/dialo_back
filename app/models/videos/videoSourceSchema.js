import mongoose from 'mongoose';

const VideoSourceSchema = mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  uri: {
    type: String,
    required: true
  }
});

export default VideoSourceSchema;

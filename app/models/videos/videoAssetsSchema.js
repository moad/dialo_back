import mongoose from 'mongoose';

const VideoAssetsSchema = mongoose.Schema({
  iframe: {
    type: String,
    required: true
  },
  player: {
    type: String,
    required: true
  },
  hls: {
    type: String,
    required: true
  },
  thumbnail: {
    type: String,
    required: true
  }
});

export default VideoAssetsSchema;

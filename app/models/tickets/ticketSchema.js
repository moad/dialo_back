import mongoose, { Schema } from 'mongoose';
import moment from 'moment';

const ticketSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 80
    },
    category: {
      type: String,
      enum: ['bug', 'user', 'content', 'feature', 'improvment'],
      required: true,
      searchable: true
    },
    status: {
      type: String,
      enum: ['broadcast', 'open', 'in progress', 'close'],
      required: true,
      searchable: true,
      default: 'broadcast'
    },
    description: {
      type: String,
      required: true,
      minlength: 100
    },
    idOwner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    creationDate: {
      type: Date,
      default: Date.now
    },
    lastUpdateDate: {
      type: Date,
      default: Date.now
    },
    closedDate: {
      type: Date,
      default: null
    },
    timestamp: {
      type: Number,
      default: moment().unix(1318874398806)
    }
  },
  {
    strict: true
  }
);

const Ticket = mongoose.model('Ticket', ticketSchema);

export default Ticket;

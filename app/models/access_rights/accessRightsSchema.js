import mongoose from 'mongoose';
import Schema from 'mongoose';

const AccessRightSchema = mongoose.Schema({
  idOwner: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  idDocument: {
    type: Schema.Types.ObjectId,
    ref: 'Document',
    required: true,
    searchable: true
  },
  writeAccess: {
    type: [String],
    ref: 'User'
  },
  deleteAccess: {
    type: [String],
    ref: 'User'
  }
});

const AccessRight = mongoose.model('AccessRight', AccessRightSchema);

export default AccessRight;

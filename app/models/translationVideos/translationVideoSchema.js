import mongoose from 'mongoose';
import Schema from 'mongoose';
import moment from 'moment';
import mongoosePaginate from 'mongoose-paginate';
import VideoSourceSchema from 'models/videos/videoSourceSchema';
import VideoAssetsSchema from 'models/videos/videoAssetsSchema';

const TranslationVideoSchema = mongoose.Schema(
  {
    idOwner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      default: null
    },
    public: {
      type: Boolean,
      default: false
    },
    publishedAt: {
      type: String,
      default: null
    },
    tags: {
      type: [String],
      default: []
    },
    metadata: {
      type: [String],
      default: []
    },
    source: {
      type: VideoSourceSchema,
      default: {}
    },
    assets: {
      type: VideoAssetsSchema,
      default: {}
    },
    timestamp: {
      type: Number,
      default: moment().unix(1318874398806)
    }
  },
  {
    strict: true
  }
);

TranslationVideoSchema.plugin(mongoosePaginate);
const TranslationVideo = mongoose.model(
  'TranslationVideo',
  TranslationVideoSchema
);

export default TranslationVideo;

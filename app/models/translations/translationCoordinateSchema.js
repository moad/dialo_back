import mongoose from 'mongoose';
import Schema from 'mongoose';

const TranslationCoordinateSchema = mongoose.Schema({
  x: {
    type: String
  },
  y: {
    type: String
  },
  width: {
    type: String
  },
  height: {
    type: String
  },
  idVideo: {
    type: Schema.Types.ObjectId,
    ref: 'Video'
  },
  _id: false
});

export default TranslationCoordinateSchema;

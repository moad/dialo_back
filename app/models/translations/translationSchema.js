import mongoose from 'mongoose';
import moment from 'moment';
import Schema from 'mongoose';

import TranslationCoordinateSchema from 'models/translations/translationCoordinateSchema';

const TranslationSchema = mongoose.Schema(
  {
    idDocument: {
      type: Schema.Types.ObjectId,
      ref: 'Document'
    },
    translation: {
      type: [[TranslationCoordinateSchema]],
      default: []
    },
    timestamp: {
      type: Number,
      default: moment().unix(1318874398806)
    },
    certificated: {
      type: Boolean,
      default: false
    },
    grade: {
      type: Number,
      default: null
    }
  },
  {
    strict: true
  }
);

const Translation = mongoose.model('translation', TranslationSchema);

export default Translation;

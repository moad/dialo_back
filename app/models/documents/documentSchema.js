import mongoose, { Schema } from 'mongoose';
import moment from 'moment';
import mongoosePaginate from 'mongoose-paginate';

import DocumentFeaturesSchema from 'models/documents/documentFeaturesSchema';

const DocumentSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 25,
      searchable: true
    },
    link: {
      type: String,
      required: true
    },
    usageName: {
      type: String,
      default: null
    },
    nbPage: {
      type: Number,
      required: true
    },
    public: {
      type: Boolean,
      default: false
    },
    description: {
      type: String,
      default: null,
      searchable: true
    },
    status: {
      type: Number,
      default: -1
    },
    category: {
      type: String,
      enum: [
        'health',
        'finance',
        'administrative',
        'entertainment',
        'business'
      ],
      default: 'entertainment',
      searchable: true
    },
    idTranslation: {
      type: Schema.Types.ObjectId,
      ref: 'Translation',
      default: null
    },
    idOwner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    creationDate: {
      type: Date,
      default: Date.now
    },
    editDate: {
      type: Date,
      default: null
    },
    timestamp: {
      type: Number,
      default: moment().unix(1318874398806)
    },
    features: {
      type: DocumentFeaturesSchema,
      default: {}
    }
  },
  {
    strict: true
  }
);

DocumentSchema.plugin(mongoosePaginate);
const Document = mongoose.model('Document', DocumentSchema);

export default Document;

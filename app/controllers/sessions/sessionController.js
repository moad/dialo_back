import UserCreateSessionContext from 'contexts/sessions/userCreateSessionContext';
import SessionSerializer from 'serializers/users/sessionSerializer';

class SessionController {
  static async create(req, res, next) {
    let user;
    try {
      user = await UserCreateSessionContext.call(req);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [SessionSerializer({ user })],
      includes: []
    });
  }
  static async createPassportSession(req, res, next) {
    let user;
    try {
      user = await UserCreateSessionContext.callPassport(req);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [SessionSerializer({ user })],
      includes: []
    });
  }
}

export default SessionController;

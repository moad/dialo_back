import TranslationVideoSerializer from 'serializers/translationVideos/translationVideoSerializer';
import TranslationVideoCreateContext from 'contexts/translationVideos/translationVideoCreateContext';
import VideoSaveContext from 'contexts/videos/videoSaveContext';
import VideoDeleteOneContext from 'contexts/videos/videoDeleteOneContext';
import PaginationHelper from 'helpers/pagination/paginationHelper';
import TranslationVideoManager from 'modules/translationVideoManager';
import AccessRightCreateContext from 'contexts/access_rights/accessRightCreateContext';
import AccessRightManager from 'modules/accessRightManager';
import PaginationSerializer from 'serializers/paginations/paginationSerializer';

import moment from 'moment';


import {
  ApiNotFoundError,
  ApiForbiddenError,
  ApiUnauthorizedError,
  ApiBadRequestError
} from 'modules/apiError';

class TranslationVideoController {
  static async create(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (
      !req.body.title ||
      !req.body.description ||
      !req.body.public ||
      !req.body.tags ||
      !req.body.metadata ||
      !req.body.source ||
      !req.body.assets
    ) {
      return next(new ApiBadRequestError('All field should be set'));
    }
    try {
      video = await TranslationVideoCreateContext.call(req);
    } catch (err) {
      return next(err);
    }
    let accessParams = {
      idOwner: req.user._id,
      idDocument: video.id
    };
    try {
      await AccessRightCreateContext.call(accessParams);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [TranslationVideoSerializer(video)],
      includes: []
    });
  }

  static async getById(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(video = await TranslationVideoManager.findById(req.params.idVideo))) {
      return next(new ApiNotFoundError('Resource not found'));
    }
    return res.json({
      data: [TranslationVideoSerializer(video)],
      includes: []
    });
  }

  static async getMyTranslationVideos(req, res, next) {
    let video;

    if (!req.user || !req.user._id) {
      return next(new ApiForbiddenError('User not found'));
    }
    const pagination = await PaginationHelper.setDefaultPagination(req.query);
    let options = await PaginationHelper.parseOption(pagination, req);
    const filter = await PaginationHelper.cleanFilter({
      idOwner: req.user._id
    });
    try {
      video = await TranslationVideoManager.pagination(filter, options);
    } catch (err) {
      return next(err);
    }
    try {
      await AccessRightManager.GetAccess(video.docs, req.user._id);
    } catch (err) {
      return next(new ApiNotFoundError('Access forbidden' + err));
    }

    return res.json({
      data: video.docs.map(item => TranslationVideoSerializer(item)),
      includes: [PaginationSerializer(video)]
    });
  }

  static async updateData(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(video = await TranslationVideoManager.findById(req.params.idVideo))) {
      return next(new ApiNotFoundError('Resource not found'));
    }
    if (
      !req.body.title ||
      !req.body.description ||
      !req.body.public ||
      !req.body.tags ||
      !req.body.metadata
    ) {
      return next(new ApiBadRequestError('All field should be set'));
    }
    video.title = req.body.title;
    video.description = req.body.description;
    video.public = req.body.public;
    video.tags = req.body.tags;
    video.metadata = req.body.metadata;
    video.timestamp = moment();
    try {
      await VideoSaveContext.call(video);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [TranslationVideoSerializer(video)],
      includes: []
    });
  }

  static async delete(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(video = await TranslationVideoManager.findById(req.params.idVideo))) {
      return next(new ApiNotFoundError('Resource not found'));
    }
    try {
      await VideoDeleteOneContext.call(video, { _id: video._id });
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [],
      includes: []
    });
  }
}

export default TranslationVideoController;

import TicketCreateContext from 'contexts/tickets/ticketCreateContext';
import TicketSaveContext from 'contexts/tickets/ticketSaveContext';
import TicketManager from 'modules/ticketManager';
import TicketSerializer from 'serializers/tickets/ticketSerializer';
import moment from 'moment';

import {
  ApiUnauthorizedError,
  ApiNotFoundError,
  ApiForbiddenError
} from 'modules/apiError';

class TicketController {
  static async create(req, res, next) {
    let ticket;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!req.body.category) {
      return next(new ApiBadRequestError('Category should be set'));
    }
    if (req.body.title.length < 5 || req.body.title.length > 80) {
      return next(
        new ApiForbiddenError(
          'Title should be longer than 5 and shorter than 80 characters'
        )
      );
    }
    if (req.body.description.length < 100) {
      return next(
        new ApiForbiddenError(
          'Description should be longer than 100 characters'
        )
      );
    }
    let givenParams = {
      title: req.body.title,
      category: req.body.category,
      description: req.body.description,
      idOwner: req.user._id
    };
    try {
      ticket = await TicketCreateContext.call(givenParams);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [TicketSerializer(ticket)],
      includes: []
    });
  }

  static async closeTicket(req, res, next) {
    let ticket;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    try {
      ticket = await TicketManager.findById(req.params.idTicket);
    } catch (err) {
      return next(new ApiNotFoundError('Resource not found'));
    }
    if (ticket.status == 'close') {
      return next(new ApiForbiddenError('Ticket already closed'));
    }
    ticket.status = 'close';
    ticket.lastUpdateDate = Date.now();
    ticket.closedDate = Date.now();
    ticket.timestamp = moment();
    try {
      await TicketSaveContext.call(ticket);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [TicketSerializer(ticket)],
      includes: []
    });
  }

  static async getById(req, res, next) {
    let tickets;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    try {
      tickets = await TicketManager.findById(req.params.idTicket);
    } catch (err) {
      return next(new ApiNotFoundError('Ticket not found'));
    }
    return res.json({
      data: [TicketSerializer(tickets)],
      includes: []
    });
  }

  static async getByCategory(req, res, next) {
    let tickets;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    try {
      tickets = await TicketManager.findByCategory(req);
    } catch (err) {
      return next(new ApiNotFoundError('Ticket not found'));
    }
    return res.json({
      data: tickets.map(item => TicketSerializer(item)),
      includes: []
    });
  }

  static async getByStatus(req, res, next) {
    let tickets;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    try {
      tickets = await TicketManager.findByStatus(req);
    } catch (err) {
      return next(new ApiNotFoundError('Ticket not found'));
    }
    return res.json({
      data: tickets.map(item => TicketSerializer(item)),
      includes: []
    });
  }
}

export default TicketController;

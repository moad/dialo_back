import UserSaveContext from 'contexts/users/userSaveContext';
import UserDeleteContext from 'contexts/users/userDeleteContext';
import UserSerializer from 'serializers/users/userSerializer';
import UserManager from 'modules/userManager';
import UserHelper from 'helpers/users/userHelper';
import NoReplyMailHelper from 'helpers/users/noreplyMailHelper';

import DocumentDeleteAllContext from 'contexts/documents/documentDeleteAllContext';
import VideoDeleteAllContext from 'contexts/videos/videoDeleteAllContext';

import DocumentManager from 'modules/documentManager';
import VideoManager from 'modules/videoManager';
import TranslationVideoManager from 'modules/translationVideoManager';

import bcrypt from 'bcrypt-nodejs';
import moment from 'moment';

import {
  ApiUnauthorizedError,
  ApiServerError,
  ApiForbiddenError,
  ApiBadRequestError
} from 'modules/apiError';

class SettingsController {
  static async updatePublic(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    req.user.profile.firstName = req.body.profile.firstName;
    req.user.profile.lastName = req.body.profile.lastName;
    req.user.profile.gender = req.body.profile.gender;
    req.user.profile.birthday = req.body.profile.birthday;
    req.user.profile.country = req.body.profile.country;
    req.user.profile.hometown = req.body.profile.hometown;
    req.user.profile.description = req.body.profile.description;
    req.user.timestamp = moment();
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }

  static async newProfilePicture(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!req.file) {
      return next(new ApiBadRequestError('File should be set'));
    }
    try {
      UserHelper.deleteCloudinaryPicture(
        req.user.profile.profilePicture.public_id
      );
    } catch (err) {
      return next(new ApiServerError('Server got an error with cloudinary'));
    }
    req.user.profile.profilePicture.url = req.file.url;
    req.user.profile.profilePicture.public_id = req.file.public_id;
    req.user.timestamp = moment();
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }

  static async setPassword(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (
      req.user.password != null &&
      req.user.profile.createdWith.provider == 'Dialogram'
    ) {
      return next(new ApiForbiddenError('Password already set'));
    }
    req.user.password = req.body.newPassword;
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    NoReplyMailHelper.mailPasswordUpdated(req.user);
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }

  static async updatePassword(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (req.body.currentPassword && !req.body.newPassword) {
      return next(
        new ApiBadRequestError('The current and new password should be set')
      );
    }
    if (!bcrypt.compareSync(req.body.currentPassword, req.user.password)) {
      return next(new ApiForbiddenError('Wrong password'));
    }
    req.user.password = req.body.newPassword;
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    NoReplyMailHelper.mailPasswordUpdated(req.user);
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }

  static async updateAccount(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!req.body.email || !req.body.nickName) {
      return next(new ApiBadRequestError('Email and nickname should be set'));
    }
    try {
      if (req.user.nickName != req.body.nickName) {
        req.user = await UserHelper.updateNickname(req);
      }
      if (req.user.email != req.body.email) {
        req.user = await UserHelper.updateEmail(req);
      }
    } catch (err) {
      return next(err);
    }
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }

  static async confirmUpdatedEmail(req, res, next) {
    let user = await UserManager.findByTokenUpdateEmail(req);

    if (!user) {
      return next(new ApiForbiddenError('User not found'));
    }
    user.email = user.tokenReset.resetEmail.updateEmail;
    user.tokenReset.resetEmail.tokenEmail = null;
    user.tokenReset.resetEmail.updateEmail = null;
    user.timestamp = moment();
    try {
      await UserSaveContext.call(user);
    } catch (err) {
      return next(err);
    }
    NoReplyMailHelper.mailEmailUpdated(user);
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }

  static async confirmAccount(req, res, next) {
    let user = await UserManager.findByTokenConfirmAccount(req);

    if (!user) {
      return next(new ApiForbiddenError('User not found'));
    }
    user.settings.confirmedAccount.confirmed = true;
    user.settings.confirmedAccount.token = null;
    try {
      await UserSaveContext.call(user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }

  static async deleteMe(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    let document = await DocumentManager.findByOwner(req.user._id);
    let video = await VideoManager.findByOwner(req.user._id);
    let translationVideo = await TranslationVideoManager.findByOwner(
      req.user._id
    );
    try {
      if (translationVideo.length > 0) {
        await VideoDeleteAllContext.call(translationVideo, {
          idOwner: req.user._id
        });
      }
      if (video.length > 0) {
        await VideoDeleteAllContext.call(video, { idOwner: req.user._id });
      }
      if (document.length > 0) {
        await DocumentDeleteAllContext.call(document, {
          idOwner: req.user._id
        });
      }
      await UserHelper.deleteCloudinaryPicture(
        req.user.profile.profilePicture.public_id
      );
      await UserDeleteContext.call(req.user, { _id: req.user._id });
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [],
      includes: []
    });
  }
}

export default SettingsController;

import UserCreateUserContext from 'contexts/users/userCreateUserContext';
import UserCreateSessionContext from 'contexts/sessions/userCreateSessionContext';
import UserTokenContext from 'contexts/users/userTokenContext';
import SessionSerializer from 'serializers/users/sessionSerializer';
import UserSerializer from 'serializers/users/userSerializer';
import UserManager from 'modules/userManager';
import UserHelper from 'helpers/users/userHelper';
import NoReplyMailHelper from 'helpers/users/noreplyMailHelper';

import {
  ApiUnauthorizedError,
  ApiServerError,
  ApiNotFoundError,
  ApiForbiddenError,
  ApiBadRequestError
} from 'modules/apiError';

class UserController {
  static async create(req, res, next) {
    let user;
    let givenParams;

    if (
      !req.body.email ||
      !req.body.nickName ||
      !req.body.profile.firstName ||
      !req.body.profile.lastName ||
      !req.body.password
    ) {
      return next(
        new ApiBadRequestError('Please fill all field for creating an account')
      );
    }
    if ((user = await UserManager.findByEmail(req.body.email)) != null) {
      return next(
        new ApiForbiddenError('User with that email address already exist')
      );
    }
    if ((user = await UserManager.findByNickName(req.body.nickName)) != null) {
      return next(
        new ApiForbiddenError('User with that nickname already exist')
      );
    }
    if (req.body.password.length < 8 || req.body.password.length > 22) {
      return next(
        new ApiForbiddenError(
          'Password should be longer than 8 and shorter than 22 characters'
        )
      );
    }
    givenParams = {
      nickName: req.body.nickName,
      profile: {
        createdWith: {
          provider: 'Dialogram',
          userId: null
        },
        firstName: req.body.profile.firstName,
        lastName: req.body.profile.lastName,
        profilePicture: {
          url: UserHelper.getAvatarURL(
            req.body.profile.firstName,
            req.body.profile.lastName
          )
        }
      },
      email: req.body.email,
      password: req.body.password
    };
    try {
      user = await UserCreateUserContext.call(givenParams);
      await UserCreateSessionContext.createSession(
        user,
        UserHelper.getDeviceName(req),
        '1y',
        'Dialogram'
      );
      user = await UserTokenContext.createConfirmAccountToken(user, '1d');
      NoReplyMailHelper.mailConfirmAccount(user);
      NoReplyMailHelper.mailCreateUser(user);
    } catch (err) {
      return next(new ApiBadRequestError(err.message));
    }
    return res.json({
      data: [UserSerializer(user)],
      includes: [SessionSerializer({ user })]
    });
  }

  static async createFromFacebook(req, res, next) {
    let user;
    let cloudinaryUrl;

    if (!req.user) {
      return next(new ApiNotFoundError('User not found'));
    }
    if (!(user = await UserManager.findByFacebookProvider(req.user.id))) {
      try {
        if (
          !(cloudinaryUrl = await UserHelper.uploadPictureFromURL(
            req.user.profile.profilePicture.url
          ))
        ) {
          cloudinaryUrl = UserHelper.getAvatarURL(
            req.user.profile.firstName,
            req.user.profile.lastName
          );
        } else {
          req.user.profile.profilePicture.url =
            cloudinaryUrl.eager[0].secure_url;
          req.user.profile.profilePicture.public_id = cloudinaryUrl.public_id;
        }
      } catch (err) {
        return next(err);
      }
      try {
        user = await UserCreateUserContext.call(req.user);
        await UserCreateSessionContext.createSession(
          user,
          UserHelper.getDeviceName(req),
          '1y',
          'Facebook'
        );
        user = await UserTokenContext.createConfirmAccountToken(user, '1d');
        NoReplyMailHelper.mailConfirmAccount(user);
        NoReplyMailHelper.mailCreateUser(user);
      } catch (err) {
        return next(new ApiBadRequestError(err.message));
      }
    } else {
      try {
        await UserCreateSessionContext.createSession(
          user,
          UserHelper.getDeviceName(req),
          '1y',
          'Facebook'
        );
      } catch (err) {
        return next(err);
      }
    }
    return res.json({
      data: [UserSerializer(user)],
      includes: [SessionSerializer({ user })]
    });
  }

  static async me(req, res, next) {
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }

  static async searchUser(req, res, next) {
    let user;

    user = await UserManager.searchByNickName(req.params.nickName);
    return res.json({
      data: user.map(item => UserSerializer(item)),
      includes: []
    });
  }
}

export default UserController;

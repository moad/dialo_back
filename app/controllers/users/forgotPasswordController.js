import UserSaveContext from 'contexts/users/userSaveContext';
import UserTokenContext from 'contexts/users/userTokenContext';

import UserManager from 'modules/userManager';
import NoReplyMailHelper from 'helpers/users/noreplyMailHelper';

import { ApiUnauthorizedError, ApiForbiddenError } from 'modules/apiError';

class ForgotPasswordController {
  static async resetForgotPassword(req, res, next) {
    let user = await UserManager.findByEmail(req.body.email);
    let token;

    if (!user) {
      return next(
        new ApiForbiddenError('User with that mail address not found')
      ); //Create USERnotFound ERROR
    }
    try {
      user = await UserTokenContext.createForgotPasswordToken(user, '1d');
    } catch (err) {
      return next(err);
    }
    token = user.tokenReset.tokenPassword;
    NoReplyMailHelper.mailForgotPasswordReset(user, token);
    return res.json({
      data: [],
      includes: []
    });
  }

  static async updateForgotPasswordByToken(req, res, next) {
    let user = await UserManager.findByTokenForgotPassword(req);

    if (!user) {
      // TODO check expire date
      return next(
        new ApiUnauthorizedError(
          'Password reset token is invalid or has expired.'
        )
      );
    }
    user.sessions = [];
    user.tokenReset.tokenPassword = null;
    user.password = req.body.password;
    try {
      await UserSaveContext.call(user, req.body);
    } catch (err) {
      return next(err);
    }
    NoReplyMailHelper.mailPasswordUpdated(user);
    return res.json({
      data: [],
      includes: []
    });
  }
}

export default ForgotPasswordController;

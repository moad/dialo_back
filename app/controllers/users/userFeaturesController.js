import UserSaveContext from 'contexts/users/userSaveContext';
import UserSerializer from 'serializers/users/userSerializer';
import UserManager from 'modules/userManager';

import moment from 'moment';

import {
  ApiUnauthorizedError,
  ApiForbiddenError,
  ApiBadRequestError
} from 'modules/apiError';

class UserFeaturesController {
  static async follow(req, res, next) {
    let followedUser;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!req.body.nickName) {
      return next(new ApiBadRequestError('Follow nickname not set'));
    }
    if (
      (followedUser = await UserManager.findByNickName(req.body.nickName)) ==
      null
    ) {
      return next(new ApiForbiddenError('Follower nickName not found'));
    }
    if (followedUser.nickName == req.user.nickName) {
      return next(new ApiForbiddenError('Can not follow yourself'));
    }
    for (let i = 0; i < req.user.features.follows.length; i++) {
      if (req.user.features.follows[i]._id.equals(followedUser._id)) {
        return next(new ApiForbiddenError('Can not follow twice an user'));
      }
    }
    req.user.features.follows.push(followedUser);
    followedUser.features.followers.push(req.user);
    req.user.timestamp = moment();
    followedUser.timestamp = moment();
    try {
      await UserSaveContext.call(followedUser);
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }

  static async unfollow(req, res, next) {
    let unfollowedUser;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!req.body.nickName) {
      return next(new ApiBadRequestError('Follow nickname not set'));
    }
    if (
      (unfollowedUser = await UserManager.findByNickName(req.body.nickName)) ==
      null
    ) {
      return next(new ApiForbiddenError('Unfollower nickName not found'));
    }
    if (unfollowedUser.nickName == req.user.nickName) {
      return next(new ApiForbiddenError('Could not unfollow yourself'));
    }
    for (let i = 0; i < unfollowedUser.features.followers.length; i++) {
      if (unfollowedUser.features.followers[i]._id.equals(req.user._id)) {
        unfollowedUser.features.followers.splice(i, 1);
      }
    }
    for (let i = 0; i < req.user.features.follows.length; i++) {
      if (req.user.features.follows[i]._id.equals(unfollowedUser._id)) {
        req.user.features.follows.splice(i, 1);
      }
    }
    req.user.timestamp = moment();
    unfollowedUser.timestamp = moment();
    try {
      await UserSaveContext.call(unfollowedUser);
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [UserSerializer(req.user)],
      includes: []
    });
  }
}

export default UserFeaturesController;

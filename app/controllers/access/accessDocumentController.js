import DocumentManager from 'modules/documentManager';
import AccessRightManager from 'modules/accessRightManager';

import {
  ApiNotFoundError,
  ApiUnauthorizedError,
  ApiForbiddenError
} from 'modules/apiError';

class AccessDocumentController {
  static async addDeleteAccess(req, res, next) {
    let document;
    let loop = 0;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    try {
      document = await DocumentManager.findById(req.params.idDocument);
    } catch (err) {
      return next(new ApiNotFoundError('Resource not found'));
    }
    while (req.body.user[loop]) {
      try {
        await AccessRightManager.AddDeleteAccess(
          document.id,
          req.user._id,
          req.body.user[loop]
        );
      } catch (err) {
        return next(
          new ApiUnauthorizedError('Failed to add the specified access: ' + err)
        );
      }
      loop++;
    }
    return res.json({
      data: [],
      includes: []
    });
  }

  static async removeDeleteAccess(req, res, next) {
    let document;
    let loop = 0;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    try {
      document = await DocumentManager.findById(req.params.idDocument);
    } catch (err) {
      return next(new ApiNotFoundError('Resource not found'));
    }
    while (req.body.user[loop]) {
      try {
        await AccessRightManager.RemoveDeleteAccess(
          document.id,
          req.user._id,
          req.body.user[loop]
        );
      } catch (err) {
        return next(
          new ApiUnauthorizedError(
            'Failed to remove the specified access: ' + err
          )
        );
      }
      loop++;
    }
    return res.json({
      data: [],
      includes: []
    });
  }

  /*
   ** WRITING ACCESS FUNCTIONS.
   */
  static async addWriteAccess(req, res, next) {
    let document;
    let loop = 0;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    try {
      document = await DocumentManager.findById(req.params.idDocument);
    } catch (err) {
      return next(new ApiNotFoundError('Resource not found'));
    }
    while (req.body.user[loop]) {
      try {
        await AccessRightManager.AddWriteAccess(
          document.id,
          req.user._id,
          req.body.user[loop]
        );
      } catch (err) {
        return next(
          new ApiUnauthorizedError('Failed to add the specified access: ' + err)
        );
      }
      loop++;
    }
    return res.json({
      data: [],
      includes: []
    });
  }

  static async removeWriteAccess(req, res, next) {
    let document;
    let loop = 0;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    try {
      document = await DocumentManager.findById(req.params.idDocument);
    } catch (err) {
      return next(new ApiNotFoundError('Resource not found'));
    }

    while (req.body.user[loop]) {
      try {
        tmp = await AccessRightManager.RemoveWriteAccess(
          document.id,
          req.user._id,
          req.body.user[loop]
        );
      } catch (err) {
        return next(
          new ApiUnauthorizedError(
            'Failed to remove the specified access: ' + err
          )
        );
      }
      loop++;
    }
    return res.json({
      data: [],
      includes: []
    });
  }
}

export default AccessDocumentController;

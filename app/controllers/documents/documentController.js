import DocumentSerializer from 'serializers/documents/documentSerializer';
import DocumentCreateContext from 'contexts/documents/documentCreateContext';
import DocumentSaveContext from 'contexts/documents/documentSaveContext';
import DocumentDeleteOneContext from 'contexts/documents/documentDeleteOneContext';
import DocumentHelper from 'helpers/documents/documentHelper';
import PaginationHelper from 'helpers/pagination/paginationHelper';
import DocumentManager from 'modules/documentManager';
import AccessRightCreateContext from 'contexts/access_rights/accessRightCreateContext';
import AccessRightManager from 'modules/accessRightManager';
import PaginationSerializer from 'serializers/paginations/paginationSerializer';
import moment from 'moment';

import {
  ApiNotFoundError,
  ApiUnauthorizedError,
  ApiForbiddenError,
  ApiBadRequestError
} from 'modules/apiError';

class DocumentController {
  static async create(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!req.file) {
      return next(new ApiBadRequestError('A file should be set'));
    }
    if (
      !req.body.name ||
      !req.body.description ||
      !req.body.public ||
      !req.body.category
    ) {
      return next(new ApiBadRequestError('Fields missing'));
    }
    let givenParams = {
      link:
        global.env.server.http +
        '://' +
        global.env.server.addr +
        '/medias/' +
        req.file.filename +
        '?accessToken=' +
        req.headers.authorization.split(' ')[1],
      usageName: req.file.filename,
      name: req.body.name,
      nbPage: await DocumentHelper.getNbPages(req.file.filename),
      idOwner: req.user._id,
      description: req.body.description,
      public: req.body.public,
      category: req.body.category
    };

    try {
      document = await DocumentCreateContext.call(givenParams);
    } catch (err) {
      return next(err);
    }

    let accessParams = {
      idOwner: req.user._id,
      idDocument: document.id
    };

    try {
      await AccessRightCreateContext.call(accessParams);
    } catch (err) {
      return next(err);
    }
    document['docAccess'] = ['W', 'D'];

    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async getById(req, res, next) {
    let document;
    let tab = [];

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    try {
      document = await DocumentManager.findByIdAndPopulate(
        req.params.idDocument,
        'features.comments'
      );
    } catch (err) {
      return next(new ApiNotFoundError('Document not found'));
    }

    if (document) {
      tab.push(document);
    } else {
      return next(new ApiNotFoundError('Resource not found'));
    }

    try {
      await AccessRightManager.GetAccess(tab, req.user._id);
    } catch (err) {
      return next(new ApiNotFoundError('Resource not found' + err));
    }

    if (!tab[1] && tab[0].public == false) {
      return res.json({
        data: [],
        includes: []
      });
    } else if (tab[1].docAccess.length > 0 || tab[0].public == true) {
      document['docAccess'] = tab[1].docAccess;
      return res.json({
        data: [DocumentSerializer(document)],
        includes: []
      });
    }
  }

  static async getMyDocuments(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    const pagination = await PaginationHelper.setDefaultPagination(req.query);
    let options = await PaginationHelper.parseOption(pagination, req);
    let filter = await PaginationHelper.cleanFilter(req.query);
    filter.idOwner = req.user._id;
    try {
      document = await DocumentManager.pagination(filter, options);
    } catch (err) {
      return next(err);
    }

    try {
      await AccessRightManager.GetAccess(document.docs, req.user._id);
    } catch (err) {
      return next(new ApiNotFoundError('Access forbidden' + err));
    }

    if (document.total > 1) {
      document.docs = document.docs.filter(item => item.docAccess.length > 0);
      document.total = document.docs.length;
    }

    return res.json({
      data: document.docs.map(item => DocumentSerializer(item)),
      includes: [PaginationSerializer(document)]
    });
  }

  static async updateData(req, res, next) {
    let document;
    let tab = [];

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (
      !req.body.name ||
      !req.body.description ||
      !req.body.public ||
      !req.body.category
    ) {
      return next(new ApiBadRequestError('Fields missing'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Resource not found'));
    }

    if (
      (await AccessRightManager.GetWriteAccess(document.id, req.user._id)) ==
      false
    ) {
      return next(new ApiForbiddenError('Not enough permissions'));
    }

    if (document) {
      tab.push(document);
    } else {
      return res.json({
        data: [],
        includes: []
      });
    }

    try {
      await AccessRightManager.GetAccess(tab, req.user._id);
    } catch (err) {
      return next(new ApiForbiddenError('Access denied' + err));
    }

    if (!tab[1]) {
      return next(
        new ApiForbiddenError('User not authorized to perform this action')
      );
    }
    document.name = req.body.name;
    document.description = req.body.description;
    document.public = req.body.public;
    document.category = req.body.category;
    document.editDate = Date.now();
    document.timestamp = moment();
    try {
      await DocumentSaveContext.call(document);
    } catch (err) {
      return next(err);
    }

    if (tab[1].docAccess.length > 0 || tab[0].public == true) {
      document['docAccess'] = tab[1].docAccess;
      return res.json({
        data: [DocumentSerializer(document)],
        includes: []
      });
    }

    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async delete(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Resource not found'));
    }
    if (
      (await AccessRightManager.GetDeleteAccess(document.id, req.user._id)) ==
      false
    ) {
      return next(new ApiForbiddenError('Not enough permissions'));
    }

    try {
      await DocumentDeleteOneContext.call(document, { _id: document._id });
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [],
      includes: []
    });
  }

  static async getAllDocuments(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    const pagination = await PaginationHelper.setDefaultPagination(req.query);
    let options = await PaginationHelper.parseOption(pagination, req);
    const filter = await PaginationHelper.cleanFilter(req.query);
    let docNb = 0;

    try {
      document = await DocumentManager.pagination(filter, options);
    } catch (err) {
      return next(err);
    }
    docNb = document.docs.length
    try {
      await AccessRightManager.GetAccess(document.docs, req.user._id);
    } catch (err) {
      return next(new ApiNotFoundError('Access forbidden' + err));
    }

    document.docs = document.docs.filter(
      item => item.docAccess.length > 0 || item.public == true
    );
    document.total = document.docs.length;

    return res.json({
      data: document.docs.map(item => DocumentSerializer(item)),
      includes: [PaginationSerializer(document)]
    });
  }
}

export default DocumentController;

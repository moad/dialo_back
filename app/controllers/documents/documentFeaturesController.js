import DocumentSerializer from 'serializers/documents/documentSerializer';
import CommentCreateContext from 'contexts/features/comments/commentCreateContext';
import CommentSaveContext from 'contexts/features/comments/commentSaveContext';
import CommentDeleteContext from 'contexts/features/comments/commentDeleteContext';
import DocumentSaveContext from 'contexts/documents/documentSaveContext';
import UserSaveContext from 'contexts/users/userSaveContext';
import DocumentManager from 'modules/documentManager';
import CommentManager from 'modules/commentManager';
import moment from 'moment';

import {
  ApiNotFoundError,
  ApiUnauthorizedError,
  ApiBadRequestError,
  ApiForbiddenError
} from 'modules/apiError';

class DocumentFeaturesController {
  static async like(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    for (let i = 0; i < document.features.likes.length; i++) {
      if (document.features.likes[i]._id.equals(req.user._id)) {
        return next(new ApiForbiddenError('Can not like twice'));
      }
    }
    req.user.features.documentsLiked.push(document);
    document.features.likes.push(req.user._id);
    req.user.timestamp = moment();
    document.timestamp = moment();
    try {
      await UserSaveContext.call(req.user);
      await DocumentSaveContext.call(document);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async unlike(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (
      !(document = await DocumentManager.findOneByLike(
        req.params.idDocument,
        req.user._id
      ))
    ) {
      return next(new ApiNotFoundError('Document liked not found'));
    }
    for (let i = 0; i < document.features.likes.length; i++) {
      if (document.features.likes[i]._id.equals(req.user._id)) {
        document.features.likes.splice(i, 1);
      }
    }
    for (let i = 0; i < req.user.features.documentsLiked.length; i++) {
      if (req.user.features.documentsLiked[i]._id.equals(document._id)) {
        req.user.features.documentsLiked.splice(i, 1);
      }
    }
    req.user.timestamp = moment();
    document.timestamp = moment();
    try {
      await UserSaveContext.call(req.user);
      await DocumentSaveContext.call(document);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async favorite(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    for (let i = 0; i < document.features.favorites.length; i++) {
      if (document.features.favorites[i]._id.equals(req.user._id)) {
        return next(new ApiForbiddenError('Can not favorite twice'));
      }
    }
    req.user.features.documentsFavorite.push(document);
    document.features.favorites.push(req.user._id);
    req.user.timestamp = moment();
    document.timestamp = moment();
    try {
      await UserSaveContext.call(req.user);
      await DocumentSaveContext.call(document);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async unfavorite(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (
      !(document = await DocumentManager.findOneByFavorite(
        req.params.idDocument,
        req.user._id
      ))
    ) {
      return next(new ApiNotFoundError('Document favorite not found'));
    }
    for (let i = 0; i < document.features.favorites.length; i++) {
      if (document.features.favorites[i]._id.equals(req.user._id)) {
        document.features.favorites.splice(i, 1);
      }
    }
    for (let i = 0; i < req.user.features.documentsFavorite.length; i++) {
      if (req.user.features.documentsFavorite[i]._id.equals(document._id)) {
        req.user.features.documentsFavorite.splice(i, 1);
      }
    }
    req.user.timestamp = moment();
    document.timestamp = moment();
    try {
      await UserSaveContext.call(req.user);
      await DocumentSaveContext.call(document);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async comment(req, res, next) {
    let document;
    let comment;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    if (!req.body.comment) {
      return next(new ApiBadRequestError('A comment should be set'));
    }
    const givenParams = {
      comment: req.body.comment,
      ownerId: req.user._id
    };
    try {
      comment = await CommentCreateContext.call(givenParams);
    } catch (err) {
      return next(err);
    }
    req.user.features.documentsCommented.push(document);
    document.features.comments.push(comment);
    req.user.timestamp = moment();
    document.timestamp = moment();
    try {
      await DocumentSaveContext.call(document);
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    if (
      !(document = await DocumentManager.findByIdAndPopulate(
        req.params.idDocument,
        'features.comments'
      ))
    ) {
      return next(new ApiNotFoundError('Document not found'));
    }
    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async editComment(req, res, next) {
    let document;
    let comment;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!req.body.comment) {
      return next(new ApiBadRequestError('A comment should be set'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    if (!(comment = await CommentManager.findById(req.params.idComment))) {
      return next(new ApiNotFoundError('Comment not found'));
    }
    comment.comment = req.body.comment;
    comment.lastUpdateDate = Date.now();
    comment.edited = true;
    req.user.timestamp = moment();
    document.timestamp = moment();
    try {
      await CommentSaveContext.call(comment);
      await DocumentSaveContext.call(document);
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    if (
      !(document = await DocumentManager.findByIdAndPopulate(
        req.params.idDocument,
        'features.comments'
      ))
    ) {
      return next(new ApiNotFoundError('Document not found'));
    }
    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async deleteComment(req, res, next) {
    let document;
    let comment;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    if (!(comment = await CommentManager.findById(req.params.idComment))) {
      return next(new ApiNotFoundError('Comment not found'));
    }
    for (let i = 0; i < document.features.comments.length; i++) {
      if (
        document.features.comments[i]._id.equals(req.params.idComment) &&
        comment.ownerId.equals(req.user._id)
      ) {
        document.features.comments.splice(i, 1);
      }
    }
    for (let i = 0; i < req.user.features.documentsCommented.length; i++) {
      if (
        req.user.features.documentsCommented[i]._id.equals(document._id) &&
        comment.ownerId.equals(req.user._id)
      ) {
        req.user.features.documentsCommented.splice(i, 1);
      }
    }
    if (
      comment.ownerId.equals(req.user._id) &&
      comment._id.equals(req.params.idComment)
    ) {
      await CommentDeleteContext.call(comment, { _id: comment._id });
    }
    req.user.timestamp = moment();
    document.timestamp = moment();
    try {
      await DocumentSaveContext.call(document);
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async likeComment(req, res, next) {
    let document;
    let comment;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    if (!(comment = await CommentManager.findById(req.params.idComment))) {
      return next(new ApiNotFoundError('Comment not found'));
    }
    for (let i = 0; i < comment.likes.length; i++) {
      if (comment.likes[i]._id.equals(req.user._id)) {
        return next(new ApiForbiddenError('Can not like twice'));
      }
    }
    comment.likes.push(req.user._id);
    document.timestamp = moment();
    try {
      await DocumentSaveContext.call(document);
      await CommentSaveContext.call(comment);
    } catch (err) {
      return next(err);
    }
    if (
      !(document = await DocumentManager.findByIdAndPopulate(
        req.params.idDocument,
        'features.comments'
      ))
    ) {
      return next(new ApiNotFoundError('Document not found'));
    }
    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }

  static async unlikeComment(req, res, next) {
    let document;
    let comment;

    if (
      !(comment = await CommentManager.findOneByLike(
        req.params.idComment,
        req.user._id
      ))
    ) {
      return next(new ApiNotFoundError('Comment liked not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    for (let i = 0; i < comment.likes.length; i++) {
      if (comment.likes[i]._id.equals(req.user._id)) {
        comment.likes.splice(i, 1);
      }
    }
    document.timestamp = moment();
    try {
      await DocumentSaveContext.call(document);
      await CommentSaveContext.call(comment);
    } catch (err) {
      return next(err);
    }
    if (
      !(document = await DocumentManager.findByIdAndPopulate(
        req.params.idDocument,
        'features.comments'
      ))
    ) {
      return next(new ApiNotFoundError('Document not found'));
    }
    return res.json({
      data: [DocumentSerializer(document)],
      includes: []
    });
  }
}

export default DocumentFeaturesController;

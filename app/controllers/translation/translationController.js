import TranslationSerializer from 'serializers/translations/translationSerializer';
import TranslationCreateContext from 'contexts/translations/translationCreateContext';
import TranslationSaveContext from 'contexts/translations/translationSaveContext';
import TranslationDeleteContext from 'contexts/translations/translationDeleteContext';
import TranslationManager from 'modules/translationManager';
import DocumentManager from 'modules/documentManager';
import DocumentSaveContext from 'contexts/documents/documentSaveContext';

import moment from 'moment';

import {
  ApiNotFoundError,
  ApiBadRequestError,
  ApiUnauthorizedError
} from 'modules/apiError';

class TranslationController {
  static async create(req, res, next) {
    let translation;
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!req.body.translation) {
      return next(
        new ApiBadRequestError('Translation coordinate should be set')
      );
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Resource not found'));
    }
    req.body.idDocument = document._id;
    try {
      translation = await TranslationCreateContext.call(req.body);
      document.idTranslation = translation._id;
      await DocumentSaveContext.call(document);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [TranslationSerializer(translation)],
      includes: []
    });
  }

  static async getById(req, res, next) {
    let document;
    let translation;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    if (
      !(translation = await TranslationManager.findById(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Translation not found'));
    }
    return res.json({
      data: [TranslationSerializer(translation)],
      includes: []
    });
  }

  static async updateData(req, res, next) {
    let document;
    let translation;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    if (
      !(translation = await TranslationManager.findById(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Translation not found'));
    }
    if (!req.body.translation) {
      return next(
        new ApiBadRequestError('Translation coordinate should be set')
      );
    }
    translation.translation = [];
    for (let i = 0; i < req.body.translation.length; i++) {
      translation.translation.push(req.body.translation[i]);
    }
    translation.timestamp = moment();
    try {
      await TranslationSaveContext.call(translation);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [TranslationSerializer(translation)],
      includes: []
    });
  }

  static async delete(req, res, next) {
    let document;
    let translation;

    if (!req.user) {
      return next(new ApiForbiddenError('User not found'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document not found'));
    }
    if (
      !(translation = await TranslationManager.findById(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Translation not found'));
    }
    try {
      await TranslationDeleteContext.call(translation, {
        _id: translation._id
      });
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [],
      includes: []
    });
  }
}

export default TranslationController;

import VideoApiSerializer from 'serializers/APIvideo/videoApiSerializer';

class VideoApiController {
  static async me(req, res, next) {
    return res.json({
      data: [VideoApiSerializer(global.apiVideoCredential)],
      includes: []
    });
  }
}
export default VideoApiController;

import express from 'express';
import ApiAuthentication from 'middlewares/apiAuthentication';
import UserController from 'controllers/users/userController';
import UserFeaturesController from 'controllers/users/userFeaturesController';
import SessionController from 'controllers/sessions/sessionController';
import ForgotPasswordController from 'controllers/users/forgotPasswordController';
import DocumentController from 'controllers/documents/documentController';
import DocumentFeaturesController from 'controllers/documents/documentFeaturesController';
import SettingsController from 'controllers/users/settingsController';
import TranslationController from 'controllers/translation/translationController';
import TranslationVideoController from 'controllers/translationVideos/translationVideoController';
import VideoController from 'controllers/videos/videoController';
import VideoApiController from 'controllers/APIvideo/videoApiController';
import VideoApiAuthentication from 'middlewares/videoApiAuthentication';
import passport from 'passport';
import TicketController from 'controllers/tickets/ticketController';
import CloudinaryConfig from 'middlewares/cloudinaryConfig';
import multer from 'middlewares/multer';
import AccessDocumentController from 'controllers/access/accessDocumentController';

class Routes {
  constructor() {
    this.router = express.Router();
    this._setRoutes();
  }

  getRouter() {
    return this.router;
  }

  _setRoutes() {
    this.router
      .route('/password/reset')
      .post(ForgotPasswordController.resetForgotPassword);

    this.router
      .route('/password/reset/:token')
      .post(ForgotPasswordController.updateForgotPasswordByToken);

    this.router.route('/session').post(SessionController.create);

    this.router
      .route('/auth/facebook')
      .post(
        passport.authenticate('facebook-token', { session: false }),
        UserController.createFromFacebook
      );

    this.router
      .route('/user')
      .post(UserController.create)
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/settings/confirm/:token')
      .get(SettingsController.confirmAccount);

    this.router
      .route('/search/user/:nickName')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        UserController.searchUser
      );

    this.router
      .route('/user/settings/public')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        SettingsController.updatePublic
      )
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/settings/profilePicture')
      .post(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        CloudinaryConfig.uploadPicture().single('image'),
        SettingsController.newProfilePicture
      );

    this.router
      .route('/user/settings/account')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        SettingsController.updateAccount
      )
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/settings/account/delete')
      .delete(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        SettingsController.deleteMe
      )
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/settings/account/email/:token')
      .get(SettingsController.confirmUpdatedEmail);

    this.router
      .route('/user/settings/password')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        SettingsController.updatePassword
      )
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/settings/set/password')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        SettingsController.setPassword
      )
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/documents')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentController.getMyDocuments
      );

    this.router
      .route('/user/video')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        VideoController.getMyVideos
      );

    this.router
      .route('/user/translationVideos')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TranslationVideoController.getMyTranslationVideos
      );

    this.router
      .route('/document')
      .post(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        multer.single('file'),
        DocumentController.create
      );

    this.router
      .route('/document/:idDocument')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentController.getById
      )
      .delete(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentController.delete
      );

    this.router
      .route('/document/:idDocument/like')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.like
      );

    this.router
      .route('/document/:idDocument/unlike')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.unlike
      );

    this.router
      .route('/document/:idDocument/favorite')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.favorite
      );

    this.router
      .route('/document/:idDocument/unfavorite')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.unfavorite
      );

    this.router
      .route('/document/:idDocument/comment')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.comment
      );

    this.router
      .route('/document/:idDocument/comment/edit/:idComment')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.editComment
      );

    this.router
      .route('/document/:idDocument/uncomment/:idComment')
      .delete(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.deleteComment
      );

    this.router
      .route('/document/:idDocument/comment/:idComment/like')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.likeComment
      );

    this.router
      .route('/document/:idDocument/comment/:idComment/unlike')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.unlikeComment
      );

    this.router
      .route('/document/update/:idDocument')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentController.updateData
      );

    this.router
      .route('/search')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        DocumentController.getAllDocuments
      );

    this.router
      .route('/video')
      .post(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        VideoController.create
      );

    this.router
      .route('/video/:idVideo')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        VideoController.getById
      )
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        VideoController.updateData
      )
      .delete(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        VideoController.delete
      );

    this.router
      .route('/translationVideo')
      .post(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TranslationVideoController.create
      );

    this.router
      .route('/translationVideo/:idVideo')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TranslationVideoController.getById
      )
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TranslationVideoController.updateData
      )
      .delete(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TranslationVideoController.delete
      );

    this.router
      .route('/document/:idDocument/translation')
      .post(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TranslationController.create
      );

    this.router
      .route('/document/:idDocument/translation/:idTranslation')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TranslationController.getById
      )
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TranslationController.updateData
      )
      .delete(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TranslationController.delete
      );

    this.router
      .route('/document/:idDocument/waccess')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        AccessDocumentController.addWriteAccess
      )
      .delete(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        AccessDocumentController.removeWriteAccess
      );

    this.router
      .route('/document/:idDocument/daccess')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        AccessDocumentController.addDeleteAccess
      )
      .delete(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        AccessDocumentController.removeDeleteAccess
      );

    this.router
      .route('/apiVideo/auth')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        VideoApiAuthentication.auth,
        VideoApiController.me
      );

    this.router
      .route('/user/follow')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        UserFeaturesController.follow
      );

    this.router
      .route('/user/unfollow')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        UserFeaturesController.unfollow
      );

    this.router
      .route('/report')
      .post(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TicketController.create
      );

    this.router
      .route('/report/category/:category')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TicketController.getByCategory
      );

    this.router
      .route('/report/status/:status')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TicketController.getByStatus
      );

    this.router
      .route('/report/:idTicket')
      .get(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TicketController.getById
      );

    this.router
      .route('/report/close/:idTicket')
      .put(
        ApiAuthentication.validJwt(),
        ApiAuthentication.retrieveUser,
        TicketController.closeTicket
      );
  }
}

export default Routes;

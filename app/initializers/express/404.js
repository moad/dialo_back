class NotFound {
  static sendNotFound(req, res, next) {
    return res.status(404).send('specified resource not found');
  }
}

export default NotFound;

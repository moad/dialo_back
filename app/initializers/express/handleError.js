import {
  ApiServerError,
  ApiUnauthorizedError,
  ApiBadRequestError,
  ApiForbiddenError,
  ApiMethodNotAllowedError,
  ApiNotAcceptableError,
  ApiNotFoundError
} from 'modules/apiError';
import pe from 'initializers/prettyError';

class HandleError {
  static handleError(err, req, res, next) {
    if (global.env.__DEV__) {
      console.error(pe.render(err));
    }

    // Handling internal errors.

    if (err instanceof ApiServerError) {
      return res.status(500).send(err.message);
    }
    if (
      err instanceof ApiUnauthorizedError ||
      err.name === 'ApiUnauthorizedError'
    ) {
      return res.status(401).send(err.message);
    }

    if (
      err instanceof ApiBadRequestError ||
      err.name === 'ApiBadRequestError'
    ) {
      return res.status(400).send(err.message);
    }

    if (err instanceof ApiForbiddenError || err.name === 'ApiForbiddenError') {
      return res.status(403).send(err.message);
    }

    if (
      err instanceof ApiMethodNotAllowedError ||
      err.name === 'ApiMethodNotAllowedError'
    ) {
      return res.status(405).send(err.message);
    }

    if (err instanceof ApiNotFoundError || err.name === 'ApiNotFoundError') {
      return res.status(404).send(err.message);
    }

    if (
      err instanceof ApiNotAcceptableError ||
      err.name === 'ApiNotAcceptableError'
    ) {
      return res.status(406).send(err.message);
    }

    if (
      err.name == 'UnauthorizedError' &&
      err.inner.name == 'JsonWebTokenError'
    ) {
      return res.status(401).send('Token error: ' + err.message);
    }

    // Handling dependencies errors.
    if (err.status == 400 || err.type == 'entity.parse.failed') {
      return res.status(err.status).send('Bad request');
    }

    if (err.code == 'credentials_required') {
      return res.status(err.status).send(err.message);
    }

    return res.status(500).send('Please contact an administrator.');
  }
}

export default HandleError;

import TranslationVideo from 'models/translationVideos/translationVideoSchema';
import { ApiBadRequestError } from 'modules/apiError';

class TranslationVideoCreateContext {
  static async call(req) {
    let givenParams;

    givenParams = req.body;
    givenParams.idOwner = req.user._id;
    const translationVideo = new TranslationVideo(givenParams);
    try {
      await translationVideo.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return translationVideo;
  }
}

export default TranslationVideoCreateContext;

import AccessRight from 'models/access_rights/accessRightsSchema';
import { ApiBadRequestError } from 'modules/apiError';

class AccessRightCreateContext {
  static async call(req) {
    let accessright;

    accessright = new AccessRight(req);
    try {
      await accessright.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return accessright;
  }
}

export default AccessRightCreateContext;

import AccessRight from 'models/access_rights/accessRightsSchema';
import { ApiServerError } from 'modules/apiError';

class AccessRightDeleteContext {
  static async call(access, query) {
    await access.remove(query, err => {
      if (err) {
        throw new ApiServerError(err.message);
      }
    });
  }
}

export default AccessRightDeleteContext;

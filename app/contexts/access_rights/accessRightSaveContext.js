import { ApiBadRequestError } from 'modules/apiError';

class AccessRightSaveContext {
  static async call(accessright) {
    try {
      await accessright.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return accessright;
  }
}

export default AccessRightSaveContext;

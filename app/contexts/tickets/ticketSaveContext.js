import { ApiBadRequestError } from 'modules/apiError';

class TicketSaveContext {
  static async call(ticket) {
    try {
      await ticket.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return ticket;
  }
}

export default TicketSaveContext;

import Video from 'models/videos/videoSchema';
import { ApiBadRequestError } from 'modules/apiError';

class VideoCreateContext {
  static async call(req) {
    let givenParams;

    givenParams = req.body;
    givenParams.idOwner = req.user._id;
    const video = new Video(givenParams);
    try {
      await video.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return video;
  }
}

export default VideoCreateContext;

import jwt from 'jsonwebtoken';
import UserHelper from 'helpers/users/userHelper';
import UserManager from 'modules/userManager';
import bcrypt from 'bcrypt-nodejs';

import {
  ApiBadRequestError,
  ApiUnauthorizedError,
  ApiNotFoundError
} from 'modules/apiError';

class UserCreateSessionContext {
  static async call(req) {
    let givenParams = req.body;
    let user;

    if (!givenParams.email || !givenParams.password) {
      throw new ApiBadRequestError('Email and password should be set');
    }
    try {
      user = await UserManager.findByEmail(req.body.email);
    } catch (err) {
      throw new ApiUnauthorizedError('Wrong username or password');
    }
    if (
      user == null ||
      !bcrypt.compareSync(givenParams.password, user.password)
    ) {
      throw new ApiUnauthorizedError('Wrong username or password');
    }
    return this.createSession(
      user,
      UserHelper.getDeviceName(req),
      '1y',
      'Dialogram'
    );
  }

  static async createSession(user, device, expirationTime, provider) {
    const token = jwt.sign({ userId: user._id }, global.env.key.jwtSecret, {
      algorithm: 'HS256',
      expiresIn: expirationTime
    });

    let session = {
      token,
      provider: provider,
      device: {
        userAgent: null,
        OS: null
      }
    };
    if (device) {
      session.device.userAgent = device.userAgent;
      session.device.OS = device.OS;
    }
    if (user.sessions instanceof Array) {
      user.sessions.push(session);
    } else {
      user.sessions = [session];
    }

    try {
      await user.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return user;
  }
}

export default UserCreateSessionContext;

import TranslationManager from 'modules/translationManager';
import AccessRightManager from 'modules/accessRightManager';

import { ApiServerError } from 'modules/apiError';
import fs from 'fs';

class DocumentDeleteOneContext {
  static async call(document, query) {
    let access;
    let translation;
    let i = 0;

    try {
      fs.unlinkSync(global.env.upload.folder_path + document.usageName);
    } catch (err) {
      throw new ApiServerError(err.message);
    }
    if (
      (translation = await TranslationManager.findByIdDocument(document._id))
    ) {
      while (translation[i]) {
        await translation[i].remove({ idDocument: document._id }, err => {
          if (err) {
            throw new ApiServerError(err.message);
          }
        });
        i++;
      }
    }
    try {
      access = await AccessRightManager.findByIdDocument(document._id);
    } catch (err) {
      throw new ApiServerError(err.message);
    }
    if (access) {
      await access.remove({}, err => {
        if (err) {
          throw new ApiServerError(err.message);
        }
      });
    }
    await document.remove(query, err => {
      if (err) {
        throw new ApiServerError(err.message);
      }
    });
  }
}

export default DocumentDeleteOneContext;

import TranslationManager from 'modules/translationManager';
import AccessRightManager from 'modules/accessRightManager';

import { ApiServerError } from 'modules/apiError';
import fs from 'fs';

class DocumentDeleteAllContext {
  static async call(document, query) {
    let access;
    let translation;
    let i = 0,
      j = 0;

    while (document[i]) {
      try {
        fs.unlinkSync(global.env.upload.folder_path + document[i].usageName);
      } catch (err) {
        throw new ApiServerError(err.message);
      }
      if (
        (translation = await TranslationManager.findByIdDocument(
          document[i]._id
        ))
      ) {
        while (translation[j]) {
          await translation[j].remove({ idDocument: document._id }, err => {
            if (err) {
              throw new ApiServerError(err.message);
            }
          });
          j++;
        }
      }
      try {
        access = await AccessRightManager.findByIdDocument(document[i]._id);
      } catch (err) {
        throw new ApiServerError(err.message);
      }
      await access.remove({ idDocument: document[i]._id }, err => {
        if (err) {
          throw new ApiServerError(err.message);
        }
      });
      await document[i].remove(query, err => {
        if (err) {
          throw new ApiServerError(err.message);
        }
      });
      i++;
    }
  }
}

export default DocumentDeleteAllContext;

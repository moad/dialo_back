import Translation from 'models/translations/translationSchema';

class TranslationManager {
  static async findById(idTranslation) {
    return await Translation.findById(idTranslation);
  }

  static async findOneByIdDocument(idDocument) {
    return await Translation.findOne({ idDocument: idDocument });
  }

  static async findByIdDocument(idDocument) {
    return await Translation.find({ idDocument: idDocument });
  }
}

export default TranslationManager;

import TranslationVideo from 'models/translationVideos/translationVideoSchema';

class TranslationVideoManager {
  static async findById(videoId) {
    return await TranslationVideo.findById(videoId);
  }

  static async findByOwner(req) {
    return await TranslationVideo.find({ idOwner: req });
  }

  static async pagination(filter, option) {
    return await TranslationVideo.paginate(filter, option);
  }
}

export default TranslationVideoManager;

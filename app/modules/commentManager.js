import Comment from 'models/features/comments/commentSchema';

class UserManager {
  static async findById(commentId) {
    return await Comment.findById(commentId);
  }

  static async findOneByLike(idComment, ownerId) {
    return await Comment.findOne({ _id: idComment, likes: ownerId });
  }
}

export default UserManager;

import Ticket from 'models/tickets/ticketSchema';

class TicketManager {
  static async findById(idDocument) {
    return await Ticket.findById(idDocument);
  }

  static async findByCategory(req) {
    return await Ticket.find({ category: req.params.category });
  }

  static async findByStatus(req) {
    return await Ticket.find({ status: req.params.status });
  }

  /*static async searchByKeyword(req) {
    return (await Ticket.search(req))
  }*/
}

export default TicketManager;

import Document from 'models/documents/documentSchema';

class DocumentManager {
  static async findById(idDocument) {
    return await Document.findById(idDocument);
  }

  static async findByIdAndPopulate(idDocument, populateField) {
    return await Document.findById(idDocument).populate(populateField);
  }

  static async findOneByFavorite(idDocument, ownerId) {
    return await Document.findOne({
      _id: idDocument,
      'features.favorites': ownerId
    });
  }

  static async findOneByLike(idDocument, ownerId) {
    return await Document.findOne({
      _id: idDocument,
      'features.likes': ownerId
    });
  }

  static async findByCategory(req) {
    return await Document.find({ category: req.params.category });
  }

  static async findByOwner(req) {
    return await Document.find({ idOwner: req });
  }

  static async searchByKeyword(req) {
    return await Document.search(req);
  }

  static async pagination(filter, option) {
    return await Document.paginate(filter, option);
  }
}

export default DocumentManager;

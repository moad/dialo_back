import AccessRight from 'models/access_rights/accessRightsSchema';
import AccessRightSaveContext from 'contexts/access_rights/accessRightSaveContext';

import {
  ApiNotFoundError,
  ApiUnauthorizedError,
  ApiForbiddenError
} from 'modules/apiError';

class AccessRightManager {
  static async findById(idAccess) {
    return await AccessRight.findById(idAccess);
  }

  static async findByIdDocument(idDocument) {
    return await AccessRight.findOne({ idDocument: idDocument });
  }

  /*
   ** ACCESS STATUS FUNCTIONS.
   */
  static async GetAccess(document, idUser) {
    let docAccess = [];
    let i = 0;

    if (document.length > 1) {
      for (i = 0; document[i]; i++) {
        docAccess = [];

        if (JSON.stringify(idUser) == JSON.stringify(document[i].idOwner)) {
          docAccess = ['W', 'D'];
          document[i]['docAccess'] = docAccess;
          continue;
        }

        if (
          document[i].public == false &&
          !(await AccessRightManager.GetWriteAccess(document[i].id, idUser)) &&
          !(await AccessRightManager.GetDeleteAccess(document[i].id, idUser))
        ) {
          //document is private.
          document[i]['docAccess'] = [];
          continue;
        }

        if (
          (await AccessRightManager.GetDeleteAccess(document[i].id, idUser)) ==
          true
        ) {
          docAccess.push('D');
        }
        if (
          (await AccessRightManager.GetWriteAccess(document[i].id, idUser)) ==
          true
        ) {
          docAccess.push('W');
        }
        document[i]['docAccess'] = docAccess;
        continue;
      }
    } else {
      docAccess = [];
      if (JSON.stringify(idUser) == JSON.stringify(document[0].idOwner)) {
        docAccess = ['W', 'D'];
        document.push({ docAccess: docAccess });
        return true;
      }

      if (
        document[0].public == false &&
        !(await AccessRightManager.GetWriteAccess(document[0].id, idUser)) &&
        !(await AccessRightManager.GetDeleteAccess(document[0].id, idUser))
      ) {
        return new ApiForbiddenError('This document is private');
      }

      if (
        (await AccessRightManager.GetDeleteAccess(document[0].id, idUser)) ==
        true
      ) {
        docAccess.push('D');
      }
      if (
        (await AccessRightManager.GetWriteAccess(document[0].id, idUser)) ==
        true
      ) {
        docAccess.push('W');
      }
      document.push({ docAccess: docAccess });
      return true;
    }
    return true;
  }

  static async GetWriteAccess(idDocument, idUser) {
    let access;

    try {
      access = await AccessRight.find({ idDocument: idDocument });
    } catch (err) {
      return next('failed to get document');
    }
    if (
      JSON.stringify(idUser) == JSON.stringify(access[0].idOwner) ||
      (await access[0].writeAccess.indexOf(idUser)) >= 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  static async GetDeleteAccess(idDocument, idUser) {
    let access;

    try {
      access = await AccessRight.find({ idDocument: idDocument });
    } catch (err) {
      return next('failed to get document');
    }
    if (
      JSON.stringify(idUser) == JSON.stringify(access[0].idOwner) ||
      access[0].deleteAccess.indexOf(idUser) >= 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  /*
   ** DELETION ACCESS FUNCTIONS.
   */
  static async AddDeleteAccess(idDocument, idUser, idUserToAdd) {
    let access;

    try {
      access = await AccessRight.find({ idDocument: idDocument });
    } catch (err) {
      return next('failed to get document');
    }
    // Add authorized user to perform.
    if (
      JSON.stringify(idUser) == JSON.stringify(access[0].idOwner) &&
      !access[0].deleteAccess.includes(idUserToAdd)
    ) {
      access[0].deleteAccess.push(idUserToAdd);
      try {
        await AccessRightSaveContext.call(access[0]);
      } catch (err) {
        return err;
      }
      return true;
    } else {
      return false;
    }
  }

  static async RemoveDeleteAccess(idDocument, idUser, idUserToRemove) {
    let access;

    try {
      access = await AccessRight.find({ idDocument: idDocument });
    } catch (err) {
      return next('failed to get document');
    }

    if (access[0].deleteAccess.includes(idUserToRemove)) {
      try {
        access[0].deleteAccess.splice(
          access[0].deleteAccess.indexOf(idUserToRemove),
          1
        );
      } catch (err) {
        return err;
      }
      try {
        await AccessRightSaveContext.call(access[0]);
      } catch (err) {
        return err;
      }
      return true;
    } else {
      return false;
    }
  }

  /*
   ** WRITING ACCESS FUNCTIONS.
   */
  static async AddWriteAccess(idDocument, idUser, idUserToAdd) {
    let access;

    try {
      access = await AccessRight.find({ idDocument: idDocument }); //todo
    } catch (err) {
      return next('failed to get document');
    }
    // Add authorized user to perform.
    if (
      JSON.stringify(idUser) == JSON.stringify(access[0].idOwner) &&
      !access[0].writeAccess.includes(idUserToAdd)
    ) {
      access[0].writeAccess.push(idUserToAdd);
      try {
        await AccessRightSaveContext.call(access[0]);
      } catch (err) {
        return err;
      }
      return true;
    } else {
      return false;
    }
  }

  static async RemoveWriteAccess(idDocument, idUser, idUserToRemove) {
    let access;

    try {
      access = await AccessRight.find({ idDocument: idDocument });
    } catch (err) {
      return next('failed to get document');
    }

    if (access[0].writeAccess.includes(idUserToRemove)) {
      try {
        access[0].writeAccess.splice(
          access[0].writeAccess.indexOf(idUserToRemove),
          1
        );
      } catch (err) {
        return err;
      }
      try {
        await AccessRightSaveContext.call(access[0]);
      } catch (err) {
        return err;
      }
      return true;
    } else {
      return false;
    }
  }
}

export default AccessRightManager;

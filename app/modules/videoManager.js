import Video from 'models/videos/videoSchema';

class VideoManager {
  static async findById(videoId) {
    return await Video.findById(videoId);
  }

  static async findByOwner(req) {
    return await Video.find({ idOwner: req });
  }

  static async pagination(filter, option) {
    return await Video.paginate(filter, option);
  }
}

export default VideoManager;

import getPageCount from 'docx-pdf-pagecount';
import { ApiBadRequestError } from 'modules/apiError';

class DocumentHelper {
  static async getNbPages(filename) {
    let countPages = 0;

    await getPageCount(global.env.upload.folder_path + filename)
      .then(pages => {
        countPages = pages;
      })
      .catch(err => {
        throw new ApiBadRequestError(err.message);
      });
    return countPages;
  }
}

export default DocumentHelper;

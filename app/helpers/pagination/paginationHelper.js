class PaginationHelper {
  static async setDefaultPagination(query) {
    let pagination = {
      sort: query.sort,
      page: query.page,
      limit: query.limit,
      offset: query.offset
    };

    if (query == null) {
      pagination.sort = { creationDate: -1 };
      pagination.page = 1;
      pagination.limit = 50;
      pagination.offset = 0;
    }
    if (query.sort == null) {
      pagination.sort = { creationDate: -1 };
    }
    if (query.pagination == null) {
      pagination.page = 1;
    }
    if (query.limit == null) {
      pagination.limit = 50;
    }
    if (query.offset == null) {
      pagination.page = 0;
    }
    return pagination;
  }

  static async parseOption(pagination, req) {
    let options = {
      page: parseInt(pagination.page, 10),
      limit: parseInt(pagination.limit, 10),
      offset: parseInt(pagination.offset, 10)
    };

    if (req.query.sort) {
      const cleanSorted = req.query.sort.split(',');
      options.sort = { [cleanSorted[0]]: cleanSorted[1] };
    }
    return options;
  }

  static async cleanFilter(query) {
    let filter = query;

    if (query.page || query.limit || query.offset || query.sort) {
      delete filter.page;
      delete filter.limit;
      delete filter.offset;
      delete filter.sort;
    }
    return filter;
  }
}

export default PaginationHelper;

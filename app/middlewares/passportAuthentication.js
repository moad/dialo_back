import passport from 'passport';
import FacebookTokenStrategy from 'passport-facebook-token';
import UserManager from 'modules/userManager';
import UserHelper from 'helpers/users/userHelper';

import { ApiNotFoundError } from 'modules/apiError';

class PassportAuthentication {
  static facebookInitialize() {
    passport.use(
      new FacebookTokenStrategy(
        {
          clientID: global.env.facebook_auth.id,
          clientSecret: global.env.facebook_auth.secret,
          callbackURL: `${
            global.env.server.http
          }://${
            global.env.server.addr
          }/api/auth/facebook/callback`,
          profileFields: [
            'email',
            'name',
            'hometown',
            'gender',
            'birthday',
            'age_range',
            'photos'
          ],
          scope: [
            'email',
            'user_birthday',
            'user_hometown',
            'user_location',
            'user_photos',
            'user_gender',
            'user_age_range',
            'public_profile'
          ]
        },
        async (accessToken, refreshToken, fbUser, done) => {
          let user;

          if (fbUser == null) {
            return next(new ApiNotFoundError('User not found'));
          }
          if ((user = await UserManager.findByFacebookProvider(fbUser.id))) {
            // User already exist
            return done(null, user);
          }
          user = this.getUserInfo(fbUser);
          return done(null, user);
        })  
      )};

    static getUserInfo(fbUser) {
      let user;
      
      user = {
        email: fbUser._json.email,
        nickName: UserHelper.createNickname(fbUser._json.first_name, fbUser._json.last_name),
        profile: {
          createdWith: {
            provider: "Facebook",
            userId: fbUser.id,
          },
          firstName: null,
          lastName: null,
          profilePicture: {
            url: null,
            public_id: null,
          },
          birthday: null,
          gender: null,
          hometown: null,
        }
      };
      if (fbUser._json.hometown) { user.profile.hometown = fbUser._json.hometown }
      if (fbUser._json.gender) { user.profile.gender = fbUser._json.gender }
      if (fbUser._json.birthday) { user.profile.birthday = fbUser._json.birthday }
      if (fbUser.photos[0].value) { user.profile.profilePicture.url = fbUser.photos[0].value }
      if (fbUser._json.first_name) { user.profile.firstName = fbUser._json.first_name }
      if (fbUser._json.last_name) { user.profile.lastName = fbUser._json.last_name }
      return user;
    }  
}

export default PassportAuthentication;

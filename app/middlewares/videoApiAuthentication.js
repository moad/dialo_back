import moment from 'moment';
import axios from 'axios';

import { ApiUnauthorizedError, ApiForbiddenError } from 'modules/apiError';

class VideoApiAuthentication {
  static async auth(req, res, next) {
    if (!global.apiVideoCredential) {
      try {
        const auth = await VideoApiAuthentication.callAuth().then(response => {
          return Promise.resolve(response);
        });
        global.apiVideoCredential = auth.data;
        global.apiVideoCredential.timestamp = moment()
          .toDate()
          .getTime();
      } catch (err) {
        return next(new ApiUnauthorizedError(err.message));
      }
    } else if (
      global.apiVideoCredential.timestamp + 3500 <
      moment()
        .toDate()
        .getTime()
    ) {
      try {
        const auth = await VideoApiAuthentication.callAuth().then(response => {
          return Promise.resolve(response);
        });
        global.apiVideoCredential = auth.data;
        global.apiVideoCredential.timestamp = moment()
          .toDate()
          .getTime();
      } catch (err) {
        return next(new ApiUnauthorizedError(err.message));
      }
    }
    next();
  }

  static async callAuth() {
    return axios({
      method: 'post',
      url: 'https://ws.api.video/auth/api-key',
      data: {
        apiKey: global.env.api_video.key
      }
    });
  }
}

export default VideoApiAuthentication;

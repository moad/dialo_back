const sourceSerializer = source => {
  if (!source) {
    return {};
  }

  const data = {
    type: source.type,
    uri: source.uri
  };

  return data;
};

export default sourceSerializer;

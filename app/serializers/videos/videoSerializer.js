import SourceSerializer from 'serializers/videos/sourceSerializer';
import AssetsSerializer from 'serializers/videos/assetsSerializer';

const videoSerializer = translationVideo => {
  if (!translationVideo) {
    return {};
  }

  const data = {
    type: 'videos',
    id: translationVideo.id,
    idOwner: translationVideo.idOwner,
    title: translationVideo.title,
    description: translationVideo.description,
    public: translationVideo.public,
    category: translationVideo.category,
    publishedAt: translationVideo.publishedAt,
    tags: translationVideo.tags,
    metadata: translationVideo.metadata,
    source: SourceSerializer(translationVideo.source),
    assets: AssetsSerializer(translationVideo.assets),
    timestamp: translationVideo.timestamp
  };
  return data;
};

export default videoSerializer;

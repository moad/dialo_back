const assetsSerializer = assets => {
  if (!assets) {
    return {};
  }

  const data = {
    iframe: assets.iframe,
    player: assets.player,
    hls: assets.hls,
    thumbnail: assets.thumbnail
  };

  return data;
};

export default assetsSerializer;

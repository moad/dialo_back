const videoApiSerializer = auth => {
  if (!auth) {
    return {};
  }

  const data = {
    type: 'videosApi',
    accessToken: auth.access_token,
    expiresIn: auth.expires_in
  };
  return data;
};

export default videoApiSerializer;

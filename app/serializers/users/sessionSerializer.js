import UserHelper from 'helpers/users/userHelper';

const sessionSerializer = ({ user, session } = {}) => {
  if (!session && !user) {
    return {};
  }
  if (!session && (session = UserHelper.getLastSession(user)) == {}) {
    return {};
  }

  const data = {
    type: 'session',

    id: session.id,
    providerId: session.providerId,
    provider: session.provider,
    token: session.token,
    device: {
      userAgent: session.device.userAgent,
      OS: session.device.OS
    }
  };
  if (user) {
    data.user = user.id;
  }

  return data;
};

export default sessionSerializer;

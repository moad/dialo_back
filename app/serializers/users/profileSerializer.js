import PictureSerializer from 'serializers/pictures/pictureSerializer';

const profileSerializer = profile => {
  if (!profile) {
    return {};
  }

  const data = {
    registerDate: profile.registerDate,
    createdWith: {
      prodivder: profile.createdWith.provider,
      userId: profile.createdWith.userId
    },
    firstName: profile.firstName,
    lastName: profile.lastName,
    profilePicture: PictureSerializer(profile.profilePicture),
    birthday: profile.birthday,
    gender: profile.gender,
    country: profile.country,
    hometown: profile.hometown,
    description: profile.description,
    certificated: profile.certificated
  };
  return data;
};

export default profileSerializer;

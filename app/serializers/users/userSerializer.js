import ProfileSerializer from 'serializers/users/profileSerializer';
import UserFeaturesSerializer from 'serializers/users/userFeaturesSerializer';

const userSerializer = user => {
  if (!user) {
    return {};
  }

  const data = {
    type: 'user',

    id: user.id,
    nickName: user.nickName,
    profile: ProfileSerializer(user.profile),
    email: user.email,
    timestamp: user.timestamp,
    features: UserFeaturesSerializer(user.features)
  };

  return data;
};

export default userSerializer;

import TranslationCoordinateSerializer from 'serializers/translations/translationCoordinateSerializer';

const translationSerializer = translation => {
  if (!translation) {
    return {};
  }

  const data = {
    type: 'translations',
    id: translation.id,
    idDocument: translation.idDocument,
    translation: translation.translation.map(item =>
      item.map(element => TranslationCoordinateSerializer(element))
    ),
    timestamp: translation.timestamp,
    certificated: translation.certificated,
    grade: translation.grade
  };
  return data;
};

export default translationSerializer;

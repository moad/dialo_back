const translationCoordinateSerializer = translationCoordinate => {
  if (!translationCoordinate) {
    return {};
  }

  const data = {
    x: translationCoordinate.x,
    y: translationCoordinate.y,
    width: translationCoordinate.width,
    height: translationCoordinate.height,
    idVideo: translationCoordinate.idVideo
  };
  return data;
};

export default translationCoordinateSerializer;

const paginationSerializer = pagination => {
  if (!pagination) {
    return {};
  }
  const data = {
    type: 'pagination',
    total: pagination.total,
    limit: pagination.limit,
    page: pagination.page,
    pages: pagination.pages
  };
  return data;
};

export default paginationSerializer;

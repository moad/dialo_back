# Dialogram Back-End API

Dialogram is a collaborative platform, available on mobile and web, which aims to provide interpretations in French Sign Language (LSF) of media and documents.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* [NodeJS](https://nodejs.org/en/download/) - JavaScript runtime built on Chrome's V8 JavaScript engine.
* [MongoDB](https://www.mongodb.com/download-center/community) - Cross-platform document-oriented database program.

### Installing

For windows users, you should install Win-Node-Env

```
npm install -g win-node-env
```

After installing the prerequisites and cloning the project, you should install yarn.

```
npm install -g yarn
```

At the root of the project install all the dependencies

```
yarn install
```

and create a ".env" file who looks like this

```
SERVER_PORT="port_number"
SERVER_ADDR="host" // api.dialogram.fr or localhost
MONGO_ADDR="localhost"
MONGO_DB="dialogram_database"
REQUEST_LIMIT=500
JWT_SECRET="[JsonWebToken_Secret]"
CLOUDINARY_NAME="your_cloudinary_name"
CLOUDINARY_KEY="your_cloudinary_key"
CLOUDINARY_SECRET="your_cloudinary_secret"
CLOUDINARY_PROFILE_PICTURES_FOLDER="your_cloudinary_folder"
BUGSNAG_API_KEY="[YOUR_API_KEY]"
UPLOAD_FOLDER="C:\Your\Path\For\Uploaded\documents\"
```

## Running the app

```
yarn start
```

## Documentation

Please read the [documentation](https://dialogram.github.io/dialo_back_doc/) of the project.

## Running the tests

Not available.

## Deployment

Not available.

## Built With

* [Express](https://expressjs.com/fr/) - NodeJS framework

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

Not available.

## Authors

* **Dialogram Team** - [Members](https://gitlab.com/groups/Dialogram/-/group_members)

See also the list of [contributors](https://gitlab.com/Dialogram/dialo_back/graphs/master) who participated in this project.

## Main Back-End Developpers

* [David Munoz](https://gitlab.com/davidmunoz-dev)
* [Lucas Onillon](https://gitlab.com/lezim)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
